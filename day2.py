from operator import itemgetter
with open("day2_input.txt") as f:
    inpt = [(direction, int(amount)) for direction, amount
            in [num.strip().split() for num in f.readlines()]]

depth = 0
horiz = 0
aim = 0

direct = {'up': -1,
          'down': 1}
#part1
for instruction in inpt:
    direction, dist = instruction
    if direction == 'forward':
        horiz += dist
        continue
    depth += direct[direction] * dist
print(depth*horiz)

#part1 alternative
# horiz = sum(map(lambda instruct: instruct[1], filter(lambda instruct: 'forward' in instruct, inpt)))
horiz = sum(map(itemgetter(1),
                filter(lambda instruct: 'forward' in instruct, inpt)))
depth = sum(map(lambda instr: instr[1] * direct[instr[0]],
                filter(lambda instruct: 'forward' not in instruct, inpt)))

print(depth * horiz)

#part 2
depth = 0
horiz = 0
aim = 0
for instruction in inpt:
    direction, dist = instruction
    if direction == 'forward':
        horiz += dist
        depth += aim * dist
        continue
    aim += direct[direction] * dist

print(horiz * depth)
