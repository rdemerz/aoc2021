with open("day1_input.txt") as f:
    inpt = [int(num.strip()) for num in f.readlines()]
    # inpt = list(map(int, f.readlines().))

def get_increases(inpt) -> int:
    return len(list(filter(lambda x: x[0] < x[1], zip(inpt, inpt[1:]))))

#part 1

increases = len(list(filter(lambda x: x[0] < x[1], zip(inpt, inpt[1:]))))

#or
count = 0
for x, y in zip(inpt, inpt[1:]):
    count += 1 if x< y else 0
assert (count == increases)

#part2
increases = get_increases(list(map(sum, zip(inpt, inpt[1:], inpt[2:]))))

